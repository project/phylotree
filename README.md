Phylotree Extension Module
==========================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Use
 * Maintainers


INTRODUCTION
------------
This extension provides a simple file formatter for [newick][1] tree files using
the library phylotree for display.

Related paper to cite:
Stephen D. Shank, Steven Weaver and Sergei L. Kosakovsky Pond, phylotree.js - a
JavaScript library for application development and interactive data
visualization in phylogenetics, BMC Bioinformatics (07/2018),
https://doi.org/10.1186/s12859-018-2283-2

[1]: http://evolution.genetics.washington.edu/phylip/newicktree.html
[2]: https://github.com/veg/phylotree.js/tree/master


REQUIREMENTS
------------

This module requires the following:

 * Libraries API (https://www.drupal.org/project/libraries)
 * jQuery >= 1.7 (use jQuery update module)
 * d3js v3 library
 * bootstrap library
 * underscore library
 * fontawesome library



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Install the d3.js v3 library:
   download https://github.com/d3/d3/releases/download/v3.5.17/d3.zip
   and extract it to a d3js3 directory in your library directory (so
   you should have: .../sites/XXX/libraries/d3js3/d3.min.js).

 * Install the bootstrap v3 library:
   download
   https://github.com/twbs/bootstrap/releases/download/v3.3.7/bootstrap-3.3.7-dist.zip
   and extract it to a bootstrap directory in your library directory (so
   you should have: .../sites/XXX/libraries/bootstrap/bootstrap.min.js).

 * Install the underscore v1 library:
   download https://underscorejs.org/underscore-min.js
   and extract it to an underscore directory in your library directory (so
   you should have: .../sites/XXX/libraries/underscore/underscore-min.js).

 * Install the fontawesome v4 library:
   download https://fontawesome.com/v4.7.0/#modal-download
   and extract it to a fontawesome directory in your library directory (so
   you should have:
   .../sites/XXX/libraries/fontawesome/css/font-awesome.min.css).

 * Enable the module in "Admin menu > Site building > Modules" (path
   /admin/modules).


USE
---

Example 1:
 * Create or use an existing content type.

 * Add a "file" field to that content type (admin/structure/types --> manage
   fields) and filter extensions to "nwk,newick,nhx" (for instance).

 * Change the formatter of that file (manage display) to use "Phylotree".

 * Create a corresponding content with a newick file and display it.

Example 2:
 * Create or use an existing content type.

 * Add a "text" field to that content type (admin/structure/types --> manage
   fields).

 * Change the formatter of that field (manage display) to use "Phylotree".

 * Create a corresponding content and fill the text field with the URL of a
   newick file and display it.

note: the formatters can also be used in views or other places and they support
multiple trees on a same page.

Example 3:
  * Edit a text format (admin/config/content/formats) and check the filter
    "Phylotree Filter".

  * In the "Filter processing order" section, make sure the filter "Phylotree
    Filter" comes before other filters which may alter HTML tags and save.

  * Create a new content that have a (multiple lines) text field.

  * Add a tag <phylotree url="*[URL to an alignment file]*"> in the text field.

  * Select the text format that uses the "Phylotree" filter and save.

See module help (admin/tripal/extension/phylotree/help) on your Drupal site for
more details (node/branch coloration, menu customization, ...).


MAINTAINERS
-----------

Current maintainer:

 * Valentin Guignon (vguignon) - https://www.drupal.org/user/423148

Module developed with the participation of:

  * Mathieu Rouard
