<?php

/**
 * @file
 * Contains the functions used by the Phylotree module.
 */

/**
 * Phylotree filter prepare callback.
 */
function phylotree_filter_prepare($text, $filter) {
  return preg_replace('#<phylotree +([^>\\]]+?)\s*/?>#', '[filter-phylotree ${1}]', $text);
}

/**
 * Phylotree filter process callback.
 */
function phylotree_filter_process($text, $filter) {
  static $uid = 1000;

  // Check if we have a tag.
  preg_match_all(
    '#\\[filter-phylotree +([^\\]]+)\\]#',
    $text,
    $matches,
    PREG_SET_ORDER
  );
  if ($matches) {
    phylotree_load_libraries();
    $parameters = drupal_get_query_parameters();
    $focus = NULL;
    if (!empty($parameters['focus'])) {
      $focus = $parameters['focus'];
    }

    foreach ($matches as $match) {
      // Parse parameters.
      preg_match('#url="([^"]+)"#', $match[1], $tree_url);
      if ($tree_url) {
        $tree_url = $tree_url[1];
        // Check if a focus is set.
        $focus_list = phylotree_prepare_focus($focus, file_get_contents($tree_url));
        $id = 'phylotree_' . $uid;
        // Generate render element.
        $element = array(
          '#theme' => 'phylotree',
          '#newick_url' => $tree_url,
          '#tree_id' => $id,
          '#focus' => $focus_list,
        );
        $rendered = drupal_render($element);
        $text = str_replace($match[0], $rendered, $text);
      }
      preg_match('#chado="(\d+)"#', $match[1], $phylotree_id);
      if ($phylotree_id) {
        $phylotree_id = $phylotree_id[1];
        // Check if a focus is set.
        $focus_list = phylotree_prepare_focus($focus, phylotree_load_phylotree_from_chado($phylotree_id));
        $id = 'phylotree_' . $uid;
        // Generate render element.
        $element = array(
          '#theme' => 'phylotree',
          '#chado_phylotree_id' => $phylotree_id,
          '#tree_id' => $id,
          '#focus' => $focus_list,
        );
        $rendered = drupal_render($element);
        $text = str_replace($match[0], $rendered, $text);
      }
      ++$uid;
    }
  }
  return $text;
}

/**
 * Phylotree filter tips callback.
 */
function phylotree_filter_tips($filter, $format, $long = FALSE) {
  if (!$long) {
    // This string will be shown in the content add/edit form.
    return t('<em>&lt;phylotree url=... /&gt;</em> or <em>&lt;phylotree chado=... /&gt;</em> tags will be replaced by the PhyloTree viewer.');
  }
  else {
    return t('Every instance of &lt;phylotree url=... /&gt; or &lt;phylotree chado=... /&gt; tags in the input text will be replaced with a PhyloTree viewer. The viewer can be configured from the administration interface or through tag properties to override defaults. See documentation for details.');
  }
}

/**
 * Prepare leaves focus list from a focus parameter.
 *
 * Parses the remote tree and find the full leaf name of each element specified
 * in the focus parameter.
 *
 * @param string|array|null $focus
 *   A string or an array of string composing the full name or part of it of one
 *   or several tree leaves.
 * @param string $newick_data
 *   Newick data string.
 *
 * @return array
 *   An array of table names (strings).
 */
function phylotree_prepare_focus($focus, $newick_data) {
  $focus_list = '';
  if (isset($focus) && !empty($focus)) {
    // Process Newick data to retrieve corresponding label.
    if (is_array($focus)) {
      $focus_list = array();
      foreach ($focus as $focussed_node) {
        if (
          !empty($focussed_node)
          && preg_match(
            '/[\w\s\.\-]*' . $focussed_node . '[\w\s\.\-]*/',
            $newick_data,
            $matches
          )
        ) {
          $focus_list[$matches[0]] = $matches[0];
        }
      }
      if (empty($focus_list)) {
        $focus_list = '';
      }
      elseif (1 == count($focus_list)) {
        $focus_list = current($focus_list);
      }
      else {
        $focus_list = array_keys($focus_list);
      }
    }
    elseif (
      preg_match(
        '/[\w\s\.\-]*' . $focus . '[\w\s\.\-]*/',
        $newick_data,
        $matches
      )
    ) {
      $focus_list = $matches[0];
    }
  }

  return $focus_list;
}

/**
 * Loads the libraries, JS and CSS required to render Phylotree trees.
 */
function phylotree_load_libraries() {

  // Check jQuery version.
  $jquery_version = variable_get('jquery_update_jquery_version', '0.0');
  list($jq_major_version, $jq_minor_version) =
    explode('.', $jquery_version);
  // Bootstrap requires jQuery >1.9.1.
  if (9 > $jq_minor_version) {
    drupal_set_message(
      t(
        'Phylotree requires a jQuery version equal or above 1.9 (detected: @jquery_version). Change your <a href="@url">jQuery Update module settings</a> to use a higher version.',
        array(
          '@url' => url('admin/config/development/jquery_update'),
          '@jquery_version' => $jquery_version,
        )
      ),
      'error'
    );
  }

  // Load libraries.
  $libraries = array(
    'd3js3',
    'underscore',
    'bootstrap',
    'fontawesome4',
  );

  foreach ($libraries as $library_name) {
    // Get library details from Drupal.
    $library = libraries_detect($library_name);
    if (!$library) {
      // Not found.
      drupal_set_message(
        t(
          'Failed to load !library: library not found.',
          array('!library' => $library_name)
        ),
        'error'
      );
    }
    elseif (empty($library['installed'])) {
      // Library found but not installed properly.
      drupal_set_message(
        t(
          "Failed to load !library: !error<br/>\n!error_message",
          array(
            '!library' => $library_name,
            '!error' => $library['error'],
            '!error_message' => $library['error message'],
          )
        ),
        'error'
      );
    }
    else {
      // All ok, load library.
      libraries_load($library_name);
    }
  }

  // Add Phylotree CSS and JS.
  drupal_add_css(
    drupal_get_path('module', 'phylotree') . '/theme/css/phylotree.css'
  );
  drupal_add_css(
    drupal_get_path('module', 'phylotree') . '/theme/css/phylotree_bootstrap.css'
  );

  drupal_add_js(
    drupal_get_path('module', 'phylotree') . '/theme/js/phylotree.js',
    array(
      'group' => JS_DEFAULT,
      'weight' => 10,
    )
  );
}

/**
 * Converts a phylonode into a newick node.
 *
 * Function used to replace unsupported characters in newick node names and
 * add branch length when available.
 *
 * @param string $phylonode
 *   A phylonode query result object.
 *
 * @return string
 *   The newick node string.
 */
function phylotree_convert_to_newick_node($phylonode) {
  $node_label = '';
  if (isset($phylonode->label)) {
    $node_label = preg_replace('/[^\w\.\-]/', '_', $phylonode->label);
  }
  if (isset($phylonode->distance) && !empty($phylonode->distance)) {
    $node_label .= ':' . $phylonode->distance;
  }
  return $node_label;
}

/**
 * Loads a tree from Chado phylonode table.
 *
 * Loads a tree from Chado phylonode table from a given phylotree using the
 * provided phylotree_id. It only uses phylonode table which must contain
 * valid data (ie. filled left_idx and right_idx). If Tripal is installed,
 * Chado is queried using Tripal chado_query() function, otherwise Chado is
 * assumed to be installed in the "chado" PostgreSQL schema (or a "chado"
 * database in case of mySQL/mariaDB that contains a phylonode table).
 *
 * @param int $phylotree_id
 *   The chado.phylotree.phylotree_id numeric identifier.
 *
 * @return string
 *   The loaded newick tree or an empty string.
 */
function phylotree_load_phylotree_from_chado($phylotree_id) {
  $tree_data = [];
  $newick = '';

  // Check if Tripal is installed and provides the appropriate functions.
  if (module_exists('tripal_core')) {
    $sql_query = '
      SELECT
        p.left_idx AS "left_idx",
        p.right_idx AS "right_idx",
        p.label AS "label",
        p.distance AS "distance"
      FROM
        {phylonode} p
      WHERE
         p.phylotree_id = :phylotree_id
      ORDER BY p.left_idx ASC
      ;';
    $args = [':phylotree_id' => $phylotree_id];
    $results = chado_query($sql_query, $args);
    $tree_data = $results->fetchAll();
  }
  else {
    // Tripal is not available, use fallback method and assume 'chado' is the
    // chado schema name.
    $sql_query = '
      SELECT
        p.left_idx AS "left_idx",
        p.right_idx AS "right_idx",
        p.label AS "label",
        p.distance AS "distance"
      FROM
        chado.phylonode p
      WHERE
         p.phylotree_id = :phylotree_id
      ORDER BY p.left_idx ASC
      ;';
    $args = [':phylotree_id' => $phylotree_id];
    $results = db_query($sql_query, $args);
    $tree_data = $results->fetchAll();
  }

  // Did we get something?
  if ($tree_data) {
    try {
      $parents = [];
      $previous_node = array_shift($tree_data);
      foreach ($tree_data as $node) {
        if ($node->left_idx == $previous_node->left_idx + 1) {
          // Going down one position.
          $newick .= '(';
          // Keep track of parents.
          array_push($parents, $previous_node);
        }
        elseif ($node->left_idx == $previous_node->right_idx + 1) {
          // Going right one position, same line.
          $newick .= phylotree_convert_to_newick_node($previous_node) . ',';
        }
        else {
          // Going up for at least one position.
          $newick .= phylotree_convert_to_newick_node($previous_node);
          do {
            // Continue up to subtree root node which is on the left of current
            // node.
            $parent = array_pop($parents);
            $newick .= ')' . phylotree_convert_to_newick_node($parent);
          } while ($parent->right_idx < $node->left_idx - 1);
          $newick .= ',';
        }
        // Keep track of previous node.
        $previous_node = $node;
      }
      // Add last processed node label.
      $newick .= phylotree_convert_to_newick_node($previous_node);
      // And add its parents.
      while ($parent = array_pop($parents)) {
        $newick .= ')' . phylotree_convert_to_newick_node($parent);
      }
      $newick .= ';';
    }
    catch (Exception $e) {
      if (function_exists('watchdog')) {
        watchdog('phylotree', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
      else {
        throw $e;
      }
    }
  }

  return $newick;
}

/**
 * Administrative settings form.
 *
 * @return string
 *   the HTML content of the administration form.
 */
function phylotree_admin_form($form, &$form_state, $no_js_use = FALSE) {
  // Get current settings.
  $settings = variable_get('phylotree', array());

  $form = array();

  // Options.
  $width_options = array(
    '2' => t('normal'),
    '4' => t('large'),
    '8' => t('very large'),
  );
  $style_options = array(
    '0' => t('normal'),
    '1' => t('dotted'),
    '2' => t('dashed'),
  );

  // Focus color.
  $form['focus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Focus settings'),
  );
  $default_value = !empty($settings['focus']['color_code']) ?
    $settings['focus']['color_code']
    : '#ff0000';
  $form['focus']['focus_color_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Focus color'),
    '#title_display' => 'before',
    '#description' => t('Focused branches and nodes color code (a valid CSS color code).'),
    '#default_value' => $default_value,
    '#required' => FALSE,
    '#size' => 80,
    '#maxlength' => 128,
  );
  $default_value = !empty($settings['focus']['width']) ?
    $settings['focus']['width']
    : '3';
  $form['focus']['focus_width'] = array(
    '#type' => 'select',
    '#title' => t('Focused branch width'),
    '#title_display' => 'before',
    '#description' => t('Width to use to display focused branches.'),
    '#options' => $width_options,
    '#default_value' => $default_value,
    '#required' => FALSE,
  );
  $default_value = !empty($settings['focus']['style']) ?
    $settings['focus']['style']
    : '0';
  $form['focus']['focus_style'] = array(
    '#type' => 'select',
    '#title' => t('Focused branch style'),
    '#title_display' => 'before',
    '#description' => t('Drawing style of focused branch lines.'),
    '#options' => $style_options,
    '#default_value' => $default_value,
    '#required' => FALSE,
  );

  // Filter settings.
  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter settings'),
  );
  $default_value = !empty($settings['filter']['color_code']) ?
    $settings['filter']['color_code']
    : '#0000ff';
  $form['filter']['filter_color_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter color'),
    '#title_display' => 'before',
    '#description' => t('Filtered branches and nodes color code (a valid CSS color code).'),
    '#default_value' => $default_value,
    '#required' => FALSE,
    '#size' => 80,
    '#maxlength' => 128,
  );
  $default_value = !empty($settings['filter']['width']) ?
    $settings['filter']['width']
    : '2';
  $form['filter']['filter_width'] = array(
    '#type' => 'select',
    '#title' => t('Filtered branch width'),
    '#title_display' => 'before',
    '#description' => t('Width to use to display filtered branches.'),
    '#options' => $width_options,
    '#default_value' => $default_value,
    '#required' => FALSE,
  );
  $default_value = !empty($settings['filter']['style']) ?
    $settings['filter']['style']
    : '2';
  $form['filter']['filter_style'] = array(
    '#type' => 'select',
    '#title' => t('Filtered branch style'),
    '#title_display' => 'before',
    '#description' => t('Drawing style of filtered branch lines.'),
    '#options' => $style_options,
    '#default_value' => $default_value,
    '#required' => FALSE,
  );

  // Node coloration.
  $form['node_colors'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Node coloration'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  if (empty($form_state['node_color_count'])) {
    $form_state['node_color_count'] = count($settings['node_colors']);
  }

  $form['node_colors']['node_color_list'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="phylotree-node-color-fieldset-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  for ($i = 0; $i < $form_state['node_color_count']; ++$i) {
    $form['node_colors']['node_color_list'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Node coloration settings'),
    );
    $default_value = !empty($settings['node_colors'][$i]['regexp']) ?
      $settings['node_colors'][$i]['regexp']
      : '';
    $form['node_colors']['node_color_list'][$i]['regexp'] = array(
      '#type' => 'textfield',
      '#title' => t('Regular expression'),
      '#title_display' => 'before',
      '#description' => t('Regular expression used against node labels to match nodes to color.'),
      '#default_value' => $default_value,
      '#required' => FALSE,
      '#size' => 80,
      '#maxlength' => 256,
    );
    $default_value = isset($settings['node_colors'][$i]['case_insensitive']) ?
      $settings['node_colors'][$i]['case_insensitive']
      : TRUE;
    $form['node_colors']['node_color_list'][$i]['case_insensitive'] = array(
      '#type' => 'checkbox',
      '#title' => t('Case insensitive'),
      '#title_display' => 'after',
      '#description' => t('Ignore case for regular expression if checked.'),
      '#default_value' => $default_value,
    );
    $default_value = !empty($settings['node_colors'][$i]['color_code']) ?
      $settings['node_colors'][$i]['color_code']
      : '';
    $form['node_colors']['node_color_list'][$i]['color_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Color code'),
      '#title_display' => 'before',
      '#description' => t('Color code to use to color matching nodes (a valid CSS color code). Ex.: "#ff0000"'),
      '#default_value' => $default_value,
      '#required' => FALSE,
      '#size' => 80,
      '#maxlength' => 128,
    );
    $default_value = isset($settings['node_colors'][$i]['color_subtree']) ?
      $settings['node_colors'][$i]['color_subtree']
      : TRUE;
    $form['node_colors']['node_color_list'][$i]['color_subtree'] = array(
      '#type' => 'checkbox',
      '#title' => t('Color subtrees'),
      '#title_display' => 'after',
      '#description' => t('Color branches of subtrees having all their nodes matching the regular expression.'),
      '#default_value' => $default_value,
    );
  }

  $form['node_colors']['add_node_color'] = array(
    '#type' => 'submit',
    '#value' => t('Add a new node coloration'),
    '#submit' => array('phylotree_add_node_color_add_one'),
    '#ajax' => array(
      'callback' => 'phylotree_add_node_color_callback',
      'wrapper' => 'phylotree-node-color-fieldset-wrapper',
    ),
  );

  // Node menu.
  $form['node_menus'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Node menus'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  if (empty($form_state['node_menu_count'])) {
    $form_state['node_menu_count'] = count($settings['node_menus']);
  }

  $form['node_menus']['node_menu_list'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="phylotree-node-menu-fieldset-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  for ($i = 0; $i < $form_state['node_menu_count']; ++$i) {
    $form['node_menus']['node_menu_list'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Node menu item settings'),
    );
    $default_value = !empty($settings['node_menus'][$i]['regexp']) ?
      $settings['node_menus'][$i]['regexp']
      : '';
    $form['node_menus']['node_menu_list'][$i]['regexp'] = array(
      '#type' => 'textfield',
      '#title' => t('Regular expression'),
      '#title_display' => 'before',
      '#description' => t('Regular expression used against node labels to match nodes. Capturing parenthesis can be used to specify the menu item label.'),
      '#default_value' => $default_value,
      '#required' => FALSE,
      '#size' => 80,
      '#maxlength' => 256,
    );
    $default_value = isset($settings['node_menus'][$i]['case_insensitive']) ?
      $settings['node_menus'][$i]['case_insensitive']
      : TRUE;
    $form['node_menus']['node_menu_list'][$i]['case_insensitive'] = array(
      '#type' => 'checkbox',
      '#title' => t('Case insensitive'),
      '#title_display' => 'after',
      '#description' => t('Ignore case for regular expression if checked.'),
      '#default_value' => $default_value,
    );
    $default_value = !empty($settings['node_menus'][$i]['label']) ?
      $settings['node_menus'][$i]['label']
      : '';
    $form['node_menus']['node_menu_list'][$i]['menu_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Menu item label'),
      '#title_display' => 'before',
      '#description' => t('Menu item label to display. HTML tags are not supported. You can use the variables "$matches[]" to be replaced by the matching part of the regexp. Please note that "$matches[0]" will contain the full node name and "$matches[1]" and following will contain the parts matched by the parentheses in same order.'),
      '#default_value' => $default_value,
      '#required' => FALSE,
      '#size' => 80,
      '#maxlength' => 128,
    );
    $default_value = !empty($settings['node_menus'][$i]['action']) ?
      $settings['node_menus'][$i]['action']
      : '';
    $form['node_menus']['node_menu_list'][$i]['menu_action'] = array(
      '#type' => 'textarea',
      '#title' => t('Javascript action'),
      '#title_display' => 'before',
      '#description' => t('Javascript action to perform when menu item is clicked. The javascript variable "node" can be used in the code as well as the matched parts of the regexp in the "matches" variable. To open a page like a link, use "window.location.href = \'http://somepath/\' + matches[1];". See your web browser console logs for debugging errors.'),
      '#default_value' => $default_value,
      '#required' => FALSE,
    );
    $default_value = isset($settings['node_menus'][$i]['leaf_only']) ?
      $settings['node_menus'][$i]['leaf_only']
      : TRUE;
    $form['node_menus']['node_menu_list'][$i]['leaf_only'] = array(
      '#type' => 'checkbox',
      '#title' => t('Leaf only'),
      '#title_display' => 'after',
      '#description' => t('Only add menu item to matching leaves.'),
      '#default_value' => $default_value,
    );
  }

  $form['node_menus']['add_node_menu'] = array(
    '#type' => 'submit',
    '#value' => t('Add a new node menu'),
    '#submit' => array('phylotree_add_node_menu_add_one'),
    '#ajax' => array(
      'callback' => 'phylotree_add_node_menu_callback',
      'wrapper' => 'phylotree-node-menu-fieldset-wrapper',
    ),
  );

  // Save button.
  $form['save_settings'] = array(
    '#type'        => 'submit',
    '#value'       => t('Save configuration'),
    '#submit'      => array('phylotree_admin_form_submit'),
  );

  return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function phylotree_add_node_color_callback($form, $form_state) {
  return $form['node_colors']['node_color_list'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function phylotree_add_node_color_add_one($form, &$form_state) {
  $form_state['node_color_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function phylotree_add_node_menu_callback($form, $form_state) {
  return $form['node_menus']['node_menu_list'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function phylotree_add_node_menu_add_one($form, &$form_state) {
  $form_state['node_menu_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Saves Phylotree configuration changes.
 */
function phylotree_admin_form_submit($form_id, &$form_state) {
  // Get current settings.
  $settings = variable_get('phylotree', array());

  // Focus.
  $settings['focus'] = array(
    'color_code' => preg_replace('/[^\w\-#(),.%]+/', '', $form_state['values']['focus_color_code']),
    'width' => preg_replace('/\D+/', '', $form_state['values']['focus_width']),
    'style' => preg_replace('/\D+/', '', $form_state['values']['focus_style']),
  );

  // Filter.
  $settings['filter'] = array(
    'color_code' => preg_replace('/[^\w\-#(),.%]+/', '', $form_state['values']['filter_color_code']),
    'width' => preg_replace('/\D+/', '', $form_state['values']['filter_width']),
    'style' => preg_replace('/\D+/', '', $form_state['values']['filter_style']),
  );

  // Node coloration.
  $settings['node_colors'] = array();
  foreach ($form_state['values']['node_color_list'] as $node_color) {
    if (!empty($node_color['color_code'])) {
      // Get values.
      $color_code = preg_replace('/[^\w\-#(),.%]+/', '', $node_color['color_code']);
      $regexp = $node_color['regexp'];
      $case_insensitive = $node_color['case_insensitive'];
      $color_subtree = $node_color['color_subtree'];

      // Save values.
      $settings['node_colors'][] = array(
        'color_code' => $color_code,
        'regexp' => $regexp,
        'case_insensitive' => $case_insensitive,
        'color_subtree' => $color_subtree,
      );
    }
  }

  // Node menus.
  $settings['node_menus'] = array();
  foreach ($form_state['values']['node_menu_list'] as $node_menu) {
    if (!empty($node_menu['menu_label'])) {
      // Get values.
      $menu_label = $node_menu['menu_label'];
      $menu_action = $node_menu['menu_action'];
      $regexp = $node_menu['regexp'];
      $case_insensitive = $node_menu['case_insensitive'];
      $leaf_only = $node_menu['leaf_only'];

      // Save values.
      $settings['node_menus'][] = array(
        'label' => $menu_label,
        'action' => $menu_action,
        'regexp' => $regexp,
        'case_insensitive' => $case_insensitive,
        'leaf_only' => $leaf_only,
      );
    }
  }

  // Save Phylotree settings.
  variable_set('phylotree', $settings);

  drupal_set_message(t('Phylotree configuration saved.'));
}
