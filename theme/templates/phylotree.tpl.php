<?php

/**
 * @file
 * Displays Phylotree widget.
 *
 * Available variables:
 * - $newick_url: URL to the newick file.
 * - $chado_phylotree_id: phylotree_id of a phylotree in Chado.
 * - $tree_id: tree display unique identifier on the page.
 * - $focus: one or more name/label of the node(s) to focus on.
 *
 * $newick_url and $chado_phylotree_id are mutually exclusive.
 */
// Prepare styles.
// Focus style.
$focus_style = '.branch-selected{';
if (!empty($settings['focus']['color_code'])) {
  $focus_style .= 'stroke:' . $settings['focus']['color_code'] . ' !important;';
}
$stroke_width = 2;
if (!empty($settings['focus']['width'])
    && preg_match('/^\d{1,3}$/', $settings['focus']['width'])) {
  $focus_style .= 'stroke-width:' . $settings['focus']['width'] . 'px;';
  $stroke_width = $settings['focus']['width'];
}
if (!empty($settings['focus']['style'])) {
  switch ($settings['focus']['style']) {
    case '0':
      break;

    case '1':
      $focus_style .=
        "stroke-dasharray:1,"
        . (2 * $stroke_width)
        . ";stroke-linecap:round;";
      break;

    case '2':
      $focus_style .=
        'stroke-dasharray:'
        . (10 + $stroke_width)
        . ',' . (5 + $stroke_width)
        . ';';
      break;
  }
}
$focus_style .= '}.node-selected{';
if (!empty($settings['focus']['color_code'])) {
  $focus_style .= 'fill:' . $settings['focus']['color_code'] . ' !important;';
}
$focus_style .= '}';

// Filter style.
$filter_style = '.branch-tagged{';
if (!empty($settings['filter']['color_code'])) {
  $filter_style .= 'stroke:' . $settings['filter']['color_code'] . ';';
}
$stroke_width = 2;
if (!empty($settings['filter']['width'])
    && preg_match('/^\d{1,3}$/', $settings['filter']['width'])) {
  $filter_style .= 'stroke-width: ' . $settings['filter']['width'] . 'px;';
  $stroke_width = $settings['filter']['width'];
}
if (!empty($settings['filter']['style'])) {
  switch ($settings['filter']['style']) {
    case '0':
      break;

    case '1':
      $filter_style .=
        "stroke-dasharray:1,"
        . (2 * $stroke_width)
        . ";stroke-linecap:round;";
      break;

    case '2':
      $filter_style .=
        'stroke-dasharray:'
        . (10 + $stroke_width)
        . ','
        . (5 + $stroke_width)
        . ';';
      break;
  }
}
$filter_style .= '}.node-tagged{';
if (!empty($settings['filter']['color_code'])) {
  $filter_style .= 'fill:' . $settings['filter']['color_code'] . ' !important;';
}
$filter_style .= '}';

?>

<div id="<?php echo $tree_id;?>_container" class="phylotree-block-container">
  <div id="<?php echo $tree_id;?>_toolbar" class="phylotree-toolbar"></div>
  <div id="<?php echo $tree_id;?>_display_wrapper" class="phylotree-display-wrapper">
    <svg id="<?php echo $tree_id;?>_display" class="phylotree-display"></svg>
  </div>
</div>

<script>
jQuery(function($) {

  // Overrides default colors.
  var style = document.createElement('style');
  style.type = 'text/css';
  style.innerHTML = '<?php echo str_replace(["\n", "'"], ["\\\n", '"'], $focus_style . $filter_style); ?>';
  document.getElementsByTagName('head')[0].appendChild(style);

  // Setup tree layout.
  var tree = d3.layout.phylotree('#<?php echo $tree_id;?>_container')
    .options({
      'brush': false,
      'zoom': true,
      'left-right-spacing': 'fit-to-size',
      'show-scale': false
    })
    .css_classes({'tree-container': '<?php echo $tree_id;?>-container'})
    .svg(d3.select('#<?php echo $tree_id;?>_display'))
  ;
  tree.size(Drupal.phylotree.get_render_size(tree));
<?php
  // Manage coloration.
  if ($settings && $settings['node_colors']) {
?>
  tree.style_nodes(function(element, data) {
    var regexp;
<?php
    foreach ($settings['node_colors'] as $node_color) {
?>
    regexp = new RegExp(/<?php echo $node_color['regexp']?:'^$'; ?>/<?php echo (!empty($node_color['case_insensitive']) ? ", 'i'" : ''); ?>);
    if (data.name.match(regexp)) {
      element.style('fill', '<?php echo $node_color['color_code']; ?>');
    }
<?php
    }
?>
  });
  tree.style_edges(function(element, data) {
    var regexp;
<?php
    foreach ($settings['node_colors'] as $node_color) {
      if ($node_color['color_subtree']) {
?>
    regexp = new RegExp(/<?php
      echo
        (!empty($node_color['regexp']) || $node_color['regexp'] == 0) ?
        preg_replace('/(^|[^\\\\])\//', '\/', $node_color['regexp'])
        : '^$';
      ?>/<?php
      echo (!empty($node_color['case_insensitive']) ? ", 'i'" : '');
      ?>);
    if (Drupal.phylotree.match_children(data.source, regexp, true)) {
      element.style('stroke', '<?php echo $node_color['color_code']; ?>');
    }
<?php
      }
    }
?>
  });

<?php
  }
?>

  // Add toolbar buttons.
  Drupal.phylotree.append_branch_filter_widget(
    '#<?php echo $tree_id;?>_toolbar',
    '<?php echo $tree_id;?>',
    tree
  );
  Drupal.phylotree.append_vertical_expand_widgets(
    '#<?php echo $tree_id;?>_toolbar',
    '<?php echo $tree_id;?>',
    tree
  );
  Drupal.phylotree.append_horizontal_expand_widgets(
    '#<?php echo $tree_id;?>_toolbar',
    '<?php echo $tree_id;?>',
    tree
  );
  Drupal.phylotree.append_branch_sorter_widgets(
    '#<?php echo $tree_id;?>_toolbar',
    '<?php echo $tree_id;?>',
    tree
  );

  Drupal.phylotree.append_align_toggler_widget(
    '#<?php echo $tree_id;?>_toolbar',
    '<?php echo $tree_id;?>',
    tree
  );

  Drupal.phylotree.append_layout_selection_widget(
    '#<?php echo $tree_id;?>_toolbar',
    '<?php echo $tree_id;?>',
    tree
  );

<?php
  if (!empty($newick_url)) {
?>
  // Load tree.
  $.ajax({
    url: '<?php echo $newick_url; ?>',
    dataType: 'text',
    success: function (data) {
<?php
  }
  elseif (!empty($chado_phylotree_id)) {
?>
      var data = '<?php echo phylotree_load_phylotree_from_chado($chado_phylotree_id); ?>';
<?php
  }
?>
      // Remove line breaks to allow clean Newick parsing.
      tree(data.replace(/[\r\n]+/g, '')).layout();
  
      // Manage custom menu.
<?php
  if ($settings && $settings['node_menus']) {
?>
      tree.get_nodes()
        .forEach(function(tree_node) {
<?php
    foreach ($settings['node_menus'] as $node_menu) {
      if ($node_menu['regexp'] !== '') {
?>
          d3.layout.phylotree.add_custom_menu(
            tree_node,
            function (node) {
              // Menu item title.
              regexp = new RegExp(/<?php
                echo preg_replace('/(^|[^\\\\])\//', '\\/', $node_menu['regexp']);
                ?>/<?php
                echo (!empty($node_menu['case_insensitive']) ? ", 'i'" : '');
                ?>);
              var matches = node['name'].match(regexp);
              var title = '<?php echo preg_replace('/(^|[^\\\\])\'/', '\\\'', $node_menu['label']); ?>';
              // Replace matches.
              for (var i = 0; i < matches.length; ++i) {
                var re_replace = new RegExp('\\$matches\\[' + i + '\\]');
                title = title.replace(re_replace, matches[i]);
              }
              return title;
            },
            function(node) {
              return function() {
                regexp = new RegExp(/<?php
                  echo preg_replace('/(^|[^\\\\])\//', '\\/', $node_menu['regexp']);
                  ?>/<?php
                  echo (!empty($node_menu['case_insensitive']) ? ", 'i'" : '');
                  ?>);
                var matches = node['name'].match(regexp);
                try {
                  <?php echo $node_menu['action']; ?>
                }
                catch (e) {
                  console.log('ERROR: Failed to execute node menu item action.');
                  console.log(e);
                }
                // node['param'] = !node['param'];
                // d3.layout.phylotree.trigger_refresh(tree);
                console.log('bla');
                // Run action
              }
            }(tree_node),
            function(node) {
              <?php
                if (!empty($node_menu['leaf_only'])) {
              ?>
                  // Leaf only.
                  if (!d3.layout.phylotree.is_leafnode(node)) {
                    return false;
                  }
              <?php
                }
              ?>
              // Returns true if the menu item should be displayed.
              regexp = new RegExp(/<?php
                echo preg_replace('/(^|[^\\\\])\//', '\/', $node_menu['regexp']);
                ?>/<?php
                echo (!empty($node_menu['case_insensitive']) ? ", 'i'" : '');
                ?>);
              return node['name'] && node['name'].match(regexp);
            }
            // d3.layout.phylotree.is_leafnode // condition on when to display the menu
          );
<?php
      }
    }
?>
      });
<?php
  }
?>


<?php
  if (!empty($focus)) {
    if (is_array($focus)) {
?>
      // Focus.
      var node;
<?php
      foreach ($focus as $focussed_node) {
?>
      node = tree.get_node_by_name('<?php echo $focussed_node; ?>');
      tree.modify_selection(tree.path_to_root(node));
<?php
      }
    }
    else {
?>
      var node = tree.get_node_by_name('<?php echo $focus; ?>');
      tree.modify_selection(tree.path_to_root(node));
<?php
    }
  }
  if ($newick_url) {
?>
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(textStatus);
    }
  });
<?php
  }
?>
});

</script>
