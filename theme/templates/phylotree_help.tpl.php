<?php

/**
 * @file
 * Phylotree administrative help page.
 */
?>

<h3>About Phylotree</h3>
<p>
  This Drupal extension provides a simple file formatter for
  <a href="http://evolution.genetics.washington.edu/phylip/newicktree.html">
  newick tree files</a> using the library
  <a href="https://github.com/veg/phylotree.js/tree/master">phylotree</a> for
  display.
</p>

<h3>Installation and configuration</h3>
<p>
  This module requires the following:
  <ul>
    <li>Libraries API (use <a href="https://www.drupal.org/project/libraries">libraries module</a>)</li>
    <li>jQuery >= 1.7 (use <a href="https://www.drupal.org/project/jquery_update">jQuery update module</a>)</li>
    <li>d3js v3 library</li>
    <li>bootstrap library</li>
    <li>underscore library</li>
    <li>fontawesome library</li>
  </ul>
</p>

<h4>Installation</h4>
<p>
  <ul>
    <li>
      Install as you would normally install a contributed Drupal module. See:
      <a href="https://drupal.org/documentation/install/modules-themes/modules-7">https://drupal.org/documentation/install/modules-themes/modules-7</a>
      for further information.
    </li>
    <li>
      Install the d3.js v3 library: download
      <a href="https://github.com/d3/d3/releases/download/v3.5.17/d3.zip">https://github.com/d3/d3/releases/download/v3.5.17/d3.zip</a>
      and extract it to a d3js3 directory in your library directory (so you
      should have:
      <b>.../sites/</b><i>XXX</i><b>/libraries/d3js3/d3.min.js</b>).
    </li>
    <li>
      Install the bootstrap v3 library: download
      <a href="https://github.com/twbs/bootstrap/releases/download/v3.3.7/bootstrap-3.3.7-dist.zip">https://github.com/twbs/bootstrap/releases/download/v3.3.7/bootstrap-3.3.7-dist.zip</a>
      and extract it to a bootstrap directory in your library directory (so you
      should have: <b>.../sites/</b><i>XXX</i><b>/libraries/bootstrap/bootstrap.min.js</b>).
    </li>
    <li>
      Install the underscore v1 library: download
      <a href="https://underscorejs.org/underscore-min.js">https://underscorejs.org/underscore-min.js</a>
      and extract it to an underscore directory in your library directory (so
      you should have:
      <b>.../sites/</b><i>XXX</i><b>/libraries/underscore/underscore-min.js</b>).
    </li>
    <li>
      Install the fontawesome v4 library: download
      <a href="https://fontawesome.com/v4.7.0/#modal-download">https://fontawesome.com/v4.7.0/#modal-download</a>
      and extract it to a fontawesome directory in your library directory (so
      you should have:
      <b>.../sites/</b><i>XXX</i><b>/libraries/fontawesome/css/font-awesome.min.css</b>).
    </li>
  </ul>
  Where "<i>XXX</i>" is your Drupal server name reference or "default" or "all".
</p>

<h4>Configuration</h4>
<p>
  Check correct installation of libraries:
  <?php echo l(t('libraries'), 'admin/reports/libraries'); ?><br/>
  Configuration URL:
  <?php echo l(t('settings'), 'admin/tripal/extension/phylotree'); ?><br/>
</p>
<p>
  The module can work as is with defaults but you may configure tree style
  settings (optional). It is possible to highlight parts of the tree using "node
  coloration" settings. You can add as many node coloration settings as you need
  and for each, you must specify a
  <a href="https://en.wikipedia.org/wiki/Regular_expression">regular expression</a>
  (leave color code empty to remove a setting) that will be used to match node
  labels (both internal nodes and leaves). Then, you can specify the style of
  matching nodes and matching subtree banches. Rules are processed in the same
  order as displayed on the administration interface. Please remember that some
  <a href="http://www.regular-expressions.info/">regular expressions characters
  may have a specific meaning</a>, just like the "." wich will match any
  character. If you need to specify a ".", escape it like this: "\.". If you
  would like to change the default color of trees, just specify "." as the
  matching regular expression.
</p>
<p>
  You can also specify a node to highlight using the URL parameter "focus". This
  parameter is not specified on the newick URL but rather on the Drupal page
  that will contain the displayed tree. For instance, if the following page
  displays a tree
  <a href="https://www.crop-diversity.org/mgis/content/cytogenetic-characterization-wild-musa-species">https://www.crop-diversity.org/mgis/content/cytogenetic-characterization-wild-musa-species</a>,
  we can highlight by default the leaves "AF434899_Heliconia_solomonensis" and
  "Musella_lassiocarpa" by adding the parameters
  "?focus[0]=AF434899&amp;focus[1]=Musella_lassiocarpa" to the URL as shown
  here:
  <a href="https://www.crop-diversity.org/mgis/content/cytogenetic-characterization-wild-musa-species?focus[0]=AF434899&amp;focus[1]=Musella_lassiocarpa">https://www.crop-diversity.org/mgis/content/cytogenetic-characterization-wild-musa-species<b>?focus[0]=AF434899&amp;focus[1]=Musella_lassiocarpa</b></a>.<br/>
  Please note that you can specify both the full node name or just a part of the
  name as long as the remaining parts only contain alpha-numeric characters,
  "_", "-" or ".". You may also specify a single pattern like this: "focus=ITC".
</p>

<p>
  It is possible to add actions to node menus (when a node is clicked) using
  "Node menus" settings. A node menu item will be added to each node matching
  the
  <a href="https://en.wikipedia.org/wiki/Regular_expression">regular
  expression</a>.
  You can use capturing parentheses
  (<a href="http://www.regular-expressions.info/quickstart.html">see
  Grouping and Capturing</a>) to match parts of the node name and use captured
  parts for replacement in both menu item label and javascript action. For
  menu item labels, the pattern to replace will look like "$matches[...]" where
  "..." stands for a capturing group index. Index 0 contains the whole node
  name. For actions, the javascript variables "matches" can be used and the
  "node" variable is also provided and contains the node data used by
  phylotree.js to render the tree.
</p>
<p>
  For example, it you have a node with the label "ITC0287_Musa_coccinea" and use
  the matching regular expression "(ITC\d+)_?(.*)", matches[0] will contain
  "ITC0287_Musa_coccinea", macthes[1] "ITC0287" and macthes[2] "Musa_coccinea".
  You can then generate a menu label "ITC0287 page (Musa_coccinea)" using the
  following pattern: "$matches[1] page ($matches[2])". And open the
  corresponding page using the following javascript action:<br/>
  <code>window.location.href = 'https://www.crop-diversity.org/mgis/collection/01BEL084/' + matches[1];</code>.
</p>
<p>
  The way Phylotree.js allows custom menu items prevents the use of HTML.
  Therefore, it is not possible to add an HTML link (&lt;a&gt;). In order to
  mimic links, you will have to use Javascript. For instance, like in the
  example above, you can use the following code to open a page that has the
  full node label in its URL:<br/>
  <code>window.location.href = \'http://your.site.url/\' + matches[0];</code></br>
  Of course, you can also use Javascript to rewrite the URL the way you want.
  For instance:<br/>
  <code>var url = \'http://your.site.url/\' + matches[0];<b> url = url.toLocaleLowerCase();</b> window.location.href = url;</code>
</p>

<h3>Uses</h3>

<h4>Example 1</h4>
<p>
  <ul>
    <li>Create or use an existing content type.</li>
    <li>
      Add a "file" field to that content type (admin/structure/types --&gt; manage
      fields) and filter extensions to "nwk,newick,nhx" (for instance).
    </li>
    <li>
      Change the formatter of that file (manage display) to use "Phylotree".
    </li>
    <li>Create a corresponding content with a newick file and display it.</li>
  </ul>
</p>

<h4>Example 2</h4>
<p>
  <ul>
    <li>Create or use an existing content type.</li>
    <li>
      Add a "text" field to that content type (admin/structure/types --&gt; manage
      fields).
    </li>
    <li>
      Change the formatter of that field (manage display) to use "Phylotree".
    </li>
    <li>
      Create a corresponding content and fill the text field with the URL of a
      newick file and display it.
    </li>
  </ul>
  note: the formatters can also be used in views or other places and they support
  multiple trees on a same page.
</p>

<h4>Example 3</h4>
<p>
  <ul>
    <li>
      Edit a text format (admin/config/content/formats) and check the filter
      "Phylotree Filter".
    </li>
    <li>
      In the "Filter processing order" section, make sure the filter "Phylotree
      Filter" comes before other filters which may alter HTML tags and save.
    </li>
    <li>
      Create a new content that have a (multiple lines) text field.
    </li>
    <li>
      Add a tag &lt;phylotree url=&quot;<i>[URL to a newick file]</i>&quot;/&gt;
      in the text field.
    </li>
    <li>Select the text format that uses the "Phylotree" filter and save.</li>
  </ul>
  note: If you have a Chado database with trees stored in
  phylotree/phylonode tables, it is also possible to load those trees using
  their "phylotree_id":
  &lt;phylotree chado=&quot;<i>[phylotree_id]</i>&quot;/&gt;.
</p>

<h3>References</h3>
GitHub: <a href="https://github.com/veg/phylotree.js">https://github.com/veg/phylotree.js</a><br/>
<br/>
<dl>
  <dt>If you use it, please cite:</dt>
  <dd>Stephen D. Shank, Steven Weaver and Sergei L. Kosakovsky Pond,
  <a href="https://doi.org/10.1186/s12859-018-2283-2">
  phylotree.js - a JavaScript library for application development and
  interactive data visualization in phylogenetics,
  </a>
  BMC Bioinformatics (07/2018).</dd>
</dl>
